== Das Datenmodell

Samu möchte in einem ersten Schritt einen speziellen Workflow abbilden.
Auch wenn alle Teams bereits relativ autonom arbeiten können, gibt es unter Umständen gewisse Abhängigkeiten zwischen Aufgaben.
Zum Beispiel kann das Team `Einkaufswagen` erst dann neue Produktinformationen verarbeiten, wenn das 
Team `Produkt` diese Daten auch bereitstellt. Diese Abhängigkeiten will Samu explizit darstellen und 
z.B. ein Ticket für das Einkaufswagenteam erstellen, wenn das Produktteam sein Ticket abgeschlossen hat.

Dazu hat er sich ein einfaches Modell überlegt (<<datamodel-image>>). Jedes Team hat einen Workspace, der mehrere Listen mit Aufgaben hat.
Diese Listen sind entweder intern oder dienen als Outbox bzw. Inbox eines anderes Teams. 
Die Aufgaben möchte Samu später aus dem Ticketsystem in das neue Tool synchronisieren.
Am Ende stellt er sich auch eine einfache Ansicht der Aufgaben für die Teams vor, sodass
diese nicht auch mit den Informationen konfrontiert werden, die üblicherweise nur für das Projektmanagement relevant sind.

[[datamodel-image]]
.Datenmodell
[ditaa, target="entites-overview", align="center"]
....
+--------+            +-----------+
|  Team  +------------+ Workspace |
+--------+            +-----+-----+
                            |
                            | Inbox/Outbox/Internal
                            |
                            |
+--------+            +-----+-----+
| Issue  +------------+    List   |
+--------+            +-----------+
....    

Samu wift noch einen kurzen Blick auf die Dokumentation <<jhentity>> und legt seine erste Entität an:

----
jhipster entity team
----

Samu beantwortet alle Fragen, so gut es geht. Er findet das ganze allerdings nicht sehr Hip und
hat keine Lust, diese ganzen Fragen für jede Entität zu beantworten (<<img-entity-prompts>>).
Da auch schon Feierabend ist, setzt er seine Änderungen zurück und beschließt, morgen Jennifer zu fragen,
ob die CLI die einzige Möglichkeit ist, Entitäten anzulegen

[#img-entity-prompts] 
.Konfiguration einer Entität mit dem Entity Generator
image::entity-generator.png[Entity Generator]

=== JDL

Jennifer schmunzelt ein wenig und fragt, wieso er denn nicht das JDL Studio <<jdlstudio>> verwende, um dort sein
Entitätenmodell mit einer domänenspezifischen Sprache zu beschreiben. 
Als Bonus würde das Model direkt im Browser Klassendiagram gerendert. 
Neben Entitäten könne man auch ganze Anwendungen und sogar Microservices mit JDL definieren und somit vollständigt ohne CLI arbeiten (<<jdl-app-config>>).

[#jdl-app-config]
.Anwendungskonfiguration via JDL
[source, json]
----
application {
  config {
    applicationType monolith,
    baseName RijaFlow
    packageName com.gitlab.atomfrede.rijaflow,
    authenticationType jwt,
    prodDatabaseType postgresql,
    buildTool gradle,
    clientFramework angular,
    useSass true,
    enableTranslation true,
    nativeLanguage en,
    languages [en, de]
  }
}
----


Samu liest noch ein wenig Dokumentation und schreibt sein JDL Model. Er exportiert es und importiert es in seine Anwendung mit `jhipster import jdl jhipster-jdl.jh`.
Nach dem Import inspiziert Samu, was JHipster alles generiert hat. 
Neben Liquibase Changesets, um die Datenbanktabellen anzulegen, wurden alle nötigen Spring Data Repositories und Entitätsklassen mit entsprechenden JPA/Hibernate Annotationen erzeugt. 
Für jede Entität steht eine einfache CRUD Oberfläche (<<img-crud-overview>>) zur Verfügung, 
die über eine REST Api mit dem Backend kommuniziert. 
Alle diese Dinge hätten Samu sehr viel Zeit gekostet und für einen Prototypen hätte er vermutlich keine Datenbankmigrationen, 
sodass er dies später hätte nachholen müssen.
Samu pushed seinen aktuellen Stand ins Versionskontrollsystem und macht erstmal Mittagspause um sich später mit dem Betrieb in der Cloud und automatischen Builds zu beschäftigen.


[#jdl-entity-definition]
.Entitätsdefinition via JDL
[source, json]
----
entity Team { <1>
	name String required minlength(3) <2>
}

entity Workspace {
	name String required
}

entity IssueList {
	name String required
}

entity Issue {
	title String required
}

entity ListConnection {
	name String required
	type ListRole required
}

 enum ListRole {
    INBOX, OUTBOX, INTERNAL
}

relationship OneToOne { <3>
	Team{workspace(name)} to Workspace{team(name)} <4>
}

relationship OneToMany {
	IssueList to Issue{issueList(name)}
    Workspace to ListConnection{workspace(name)}
	ListConnection to IssueList{listConnection(name)}
}

paginate Team, Workspace, IssueList with pagination <5>
paginate Issue infinite-scroll

----
<1> Eine Entität benötigt einen Namen.
<2> Pro Feld können verschiedene Constraints angegeben werden, die sowohl im Backend als auch im Frontend durchgesetzt werden.
<3> Neben OneToOne und OneToMany gibt es noch ManyToMany Beziehungen.
<4> Im Standard werden in der generierten UI die IDs der referenzierten Entität angezeigt. Dies kann überschrieben werden, sodass z.B. die Namen angezeigt werden.
<5> Weitere Optionen wie die Verwendung von DTOs oder welche Art von Pagination verwendet werden soll.

[#img-entity-jdl] 
.Samus JDL Model
image::jhipster-jdl.png[JHipster JDL]

[#img-crud-overview]
.Generierte CRUD Oberfläche (Übersichtstabelle)
image::crud-overview.png[CRUD UI]

[#img-crud-detail]
.Generierte CRUD Oberfläche (Detailansicht)
image::crud-detail.png[CRUD UI Detail]


